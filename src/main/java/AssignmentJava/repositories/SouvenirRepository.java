package AssignmentJava.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import AssignmentJava.model.SouvenirModel;

public interface SouvenirRepository extends JpaRepositoryImplementation<SouvenirModel, Integer>, JpaSpecificationExecutor<SouvenirModel>{

	@Query(value= "select c.name from m_souvenir c where c.is_delete = false", nativeQuery = true)
	List<String> getAllSouvenirName();
	
	@Query(value= "select c.code from m_souvenir c where c.is_delete = false", nativeQuery = true)
	List<String> getAllSouvenirCode();
	
	@Query(value= "select * from m_souvenir c where c.is_delete = false order by c.code", nativeQuery = true)
	List<SouvenirModel> getAllSouvenir();
	
	@Query(value= "select max(id) from m_souvenir", nativeQuery = true)
	Integer getValueId();
	
	@Query("select c from SouvenirModel c where c.name =?1")
	SouvenirModel findBySouvenirName(String name);
	
}
