package AssignmentJava.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import AssignmentJava.model.RoleModel;

public interface RoleRepository extends JpaRepositoryImplementation<RoleModel, Integer>, JpaSpecificationExecutor<RoleModel>{

	@Query(value= "select c.name from m_role c where c.is_delete = false", nativeQuery = true)
	List<String> getAllRoleName();
	
	@Query(value= "select c.code from m_role c where c.is_delete = false", nativeQuery = true)
	List<String> getAllRoleCode();
	
	@Query(value= "select * from m_role c where c.is_delete = false order by c.code", nativeQuery = true)
	List<RoleModel> getAllRole();
	
	@Query(value= "select max(id) from m_role", nativeQuery = true)
	Integer getValueId();
	
	@Query("select c from RoleModel c where c.name =?1")
	RoleModel findByRoleName(String name);
	
}
