package AssignmentJava.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import AssignmentJava.model.UnitModel;

public interface UnitRepository extends JpaRepositoryImplementation<UnitModel, Integer>, JpaSpecificationExecutor<UnitModel>{

	@Query(value= "select c.name from m_unit c where c.is_delete = false", nativeQuery = true)
	List<String> getAllUnitName();
	
	@Query(value= "select c.code from m_unit c where c.is_delete = false", nativeQuery = true)
	List<String> getAllUnitCode();
	
	@Query(value= "select * from m_unit c where c.is_delete = false order by c.code", nativeQuery = true)
	List<UnitModel> getAllUnit();
	
	@Query(value= "select max(id) from m_unit", nativeQuery = true)
	Integer getValueId();
	
	@Query("select c from UnitModel c where c.name =?1")
	UnitModel findByUnitName(String name);
	
}
