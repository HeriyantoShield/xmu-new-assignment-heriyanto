package AssignmentJava.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import AssignmentJava.model.EmployeeModel;

public interface EmployeeRepository extends JpaRepository<EmployeeModel, Integer>, JpaSpecificationExecutor<EmployeeModel> {
	
	@Query(value = "select * from m_employee e where e.is_delete = false order by e.employee_number", nativeQuery = true)
	List<EmployeeModel> getAllEmployee();
	
}
