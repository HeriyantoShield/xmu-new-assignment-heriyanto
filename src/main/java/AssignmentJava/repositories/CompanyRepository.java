package AssignmentJava.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import AssignmentJava.model.CompanyModel;

public interface CompanyRepository extends JpaRepositoryImplementation<CompanyModel, Integer>, JpaSpecificationExecutor<CompanyModel> {

	@Query(value= "select c.name from m_company c where c.is_delete = false", nativeQuery = true)
	List<String> getAllCompanyName();
	
	@Query(value= "select c.code from m_company c where c.is_delete = false", nativeQuery = true)
	List<String> getAllCompanyCode();
	
	@Query(value= "select * from m_company c where c.is_delete = false order by c.code", nativeQuery = true)
	List<CompanyModel> getAllCompany();
	
	@Query(value= "select max(id) from m_company", nativeQuery = true)
	Integer getValueId();

}