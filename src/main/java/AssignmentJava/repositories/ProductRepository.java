package AssignmentJava.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import AssignmentJava.model.ProductModel;

public interface ProductRepository extends JpaRepositoryImplementation<ProductModel, Integer>, JpaSpecificationExecutor<ProductModel> {

	@Query(value= "select * from m_product c where c.is_delete = false order by c.code", nativeQuery = true)
	List<ProductModel> getAllProduct();
	
	@Query(value= "select max(id) from m_product", nativeQuery = true)
	Integer getValueId();

}