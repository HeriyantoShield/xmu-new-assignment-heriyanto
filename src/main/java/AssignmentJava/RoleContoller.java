package AssignmentJava;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import AssignmentJava.Dto.RoleSearchFormDTO;
import AssignmentJava.model.RoleModel;
import AssignmentJava.service.RoleService;

@Controller
@RequestMapping("/role")
public class RoleContoller {

	@Autowired
	private RoleService roleService;
	
	@GetMapping("index")
	public ModelAndView index(@RequestParam("page") Optional<Integer> page, @ModelAttribute RoleSearchFormDTO searchForm, HttpSession session) {
		
		ModelAndView view = new ModelAndView("/role/index");
		List<String> roleNameList = this.roleService.getAllRoleName();
		List<String> roleCodeList = this.roleService.getAllRoleCode();
		
        Integer currentPage = page.orElse(1);
		Page<RoleModel> rolePage = this.roleService.getRoleWithPage(PageRequest.of(currentPage - 1, 2), searchForm);
		
		Integer totalPages = rolePage.getTotalPages();
		if(totalPages>0) {
			List<Integer> pageNumbers = this.roleService.getPageNumbers(totalPages);
			view.addObject("pageNumbers", pageNumbers);
		}
		String message= null;
		if (session.getAttribute("message") != null) {
	    	message = (String) session.getAttribute("message");
	    }
		RoleSearchFormDTO searchFormDTO = searchForm;
		view.addObject("searchForm", searchFormDTO);
		view.addObject("roleNameList", roleNameList);
		view.addObject("roleCodeList", roleCodeList);
		view.addObject("rolePage", rolePage);
		view.addObject("message", message);
		session.removeAttribute("message");
		return view;
	}
	
	@GetMapping("add")
	public ModelAndView add() {
		ModelAndView view = new ModelAndView("/role/add");
		RoleModel role = new RoleModel();
		view.addObject("role", role);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute RoleModel role, BindingResult result, HttpSession session) {
		if(!result.hasErrors()) {
			String message = this.roleService.save(role);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/role/index");
		}
		else {
			return new ModelAndView("redirect:/role/add");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id")Integer id) {
		ModelAndView view = new ModelAndView("/role/edit");
		RoleModel role = this.roleService.getRoleFindById(id);
		String title = "Edit Role - " + role.getName() + "(" + role.getCode() + ")";
		view.addObject("role",role);
		view.addObject("title",title);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id")Integer id) {
		ModelAndView view = new ModelAndView("/role/delete");
		RoleModel role = new RoleModel();
		role.setId(id);
		view.addObject("role",role);
		return view;
	}
	
	@PostMapping("deletedata")
	public ModelAndView deletedata(@ModelAttribute RoleModel role, BindingResult result, HttpSession session) {
		if(!result.hasErrors()) {
			String message = this.roleService.delete(role);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/role/index");
		}
		else {
			return new ModelAndView("redirect:/role/delete");
		}
	}
	
	@GetMapping("details/{id}")
	public ModelAndView details(@PathVariable("id")Integer id) {
		ModelAndView view = new ModelAndView("/role/details");
		RoleModel role = this.roleService.getRoleFindById(id);
		String title = "View Role - " + role.getName() + "(" + role.getCode() + ")";
		view.addObject("role",role);
		view.addObject("title",title);
		return view;
	}
	
}
