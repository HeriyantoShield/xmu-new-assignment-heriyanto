package AssignmentJava.serviceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import AssignmentJava.Dto.SouvenirSearchFormDTO;
import AssignmentJava.Dto.UnitDTO;
import AssignmentJava.helper.SouvenirSpecifications;
import AssignmentJava.model.UnitModel;
import AssignmentJava.model.SouvenirModel;
import AssignmentJava.repositories.SouvenirRepository;
import AssignmentJava.repositories.UnitRepository;
import AssignmentJava.service.SouvenirService;

@Service
public class SouvenirServiceImpl implements SouvenirService{

	@Autowired
	private SouvenirRepository souvenirRepo;
	
	@Autowired
	private UnitRepository unitRepo;
	
	@Override
	public List<String> getAllSouvenirName() {
		return souvenirRepo.getAllSouvenirName();
	}

	@Override
	public List<String> getAllSouvenirCode() {
		return souvenirRepo.getAllSouvenirCode();
	}

	@Override
	public List<Integer> getPageNumbers(Integer totalPages) {
		List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
		return pageNumbers;
	}

	@Override
	public Page<SouvenirModel> getSouvenirWithPage(Pageable pageable, SouvenirSearchFormDTO searchForm) {
		Integer pageSize = pageable.getPageSize();
		Integer currentPage = pageable.getPageNumber();
		Integer startItem = currentPage * pageSize;
		List<SouvenirModel> souvenirList = new ArrayList<SouvenirModel>();
		if (searchForm.getSouvenirCode() != null || searchForm.getSouvenirName() != null || searchForm.getUnitName() != null || 
				searchForm.getCreatedDate() != null || searchForm.getCreatedBy() != null) {
			if (searchForm.getSouvenirCode() != null && searchForm.getSouvenirCode()!="") {
				souvenirList = this.souvenirRepo.findAll(Specification
						.where(SouvenirSpecifications.withSouvenirCode(searchForm.getSouvenirCode()))
						.and(SouvenirSpecifications.withIsDelete(false)));
			} 
			else if (searchForm.getSouvenirName() != null && searchForm.getSouvenirName()!="") {
				souvenirList = this.souvenirRepo.findAll(Specification
						.where(SouvenirSpecifications.withSouvenirName(searchForm.getSouvenirName()))
						.and(SouvenirSpecifications.withIsDelete(false)));
			} 
			else if (searchForm.getUnitName() != null) {
				souvenirList = this.souvenirRepo.findAll(Specification
						.where(SouvenirSpecifications.withSouvenirUnit(searchForm.getUnitName()))
						.and(SouvenirSpecifications.withIsDelete(false)));;
			} 
			else if (searchForm.getCreatedDate() != null) {
				souvenirList = this.souvenirRepo.findAll(Specification
						.where(SouvenirSpecifications.withCreatedDate(searchForm.getCreatedDate()))
						.and(SouvenirSpecifications.withIsDelete(false)));
			}
			else if (searchForm.getCreatedBy() != null) {
				souvenirList = this.souvenirRepo.findAll(Specification
						.where(SouvenirSpecifications.withCreatedBy(searchForm.getCreatedBy()))
						.and(SouvenirSpecifications.withIsDelete(false)));
			}
		} else {
			souvenirList = this.souvenirRepo.getAllSouvenir();
		}
		List<SouvenirModel> list;

		if (souvenirList.size() < startItem) {
			list = Collections.emptyList();
		} else {
			Integer toIndex = Math.min(startItem + pageSize, souvenirList.size());
			list = souvenirList.subList(startItem, toIndex);
		}

		Page<SouvenirModel> souvenirPage = new PageImpl<SouvenirModel>(list, PageRequest.of(currentPage, pageSize),
										   souvenirList.size());
		return souvenirPage;
	}

	@Override
	public String save(SouvenirModel souvenir) {
		String message = new String();
		SouvenirModel souvenirForDatabase = new SouvenirModel();
		souvenirForDatabase = this.souvenirRepo.findBySouvenirName(souvenir.getName());
		
		if(souvenir.getId()!=null) {
			souvenirForDatabase = this.getSouvenirFindById(souvenir.getId());
			souvenirForDatabase.setName(souvenir.getName());
			souvenirForDatabase.setmUnitId(souvenir.getmUnitId());
			souvenirForDatabase.setDescription(souvenir.getDescription());
			souvenirForDatabase.setUpdatedBy("Admin");
			souvenirForDatabase.setUpdatedDate(LocalDate.now());
			this.souvenirRepo.save(souvenirForDatabase);
			message = "Data Updated! Data Souvenir has been updated";
		}
		
		else {
			if(souvenirForDatabase==null) {
				SouvenirModel souvenirForDatabases = new SouvenirModel();
				Integer maxId = this.souvenirRepo.getValueId();
				souvenirForDatabases.setName(souvenir.getName());
				souvenirForDatabases.setDescription(souvenir.getDescription());
				souvenirForDatabases.setmUnitId(souvenir.getmUnitId());
				souvenirForDatabases.setCreatedBy("Admin");
				souvenirForDatabases.setCreatedDate(LocalDate.now());
				souvenirForDatabases.setIsDelete(false);
				if(maxId!=null && maxId<=8) {
					maxId++;
					souvenirForDatabases.setCode("SV000"+ maxId.toString());
				}
				else if(maxId!=null && maxId>=9) {
					maxId++;
					souvenirForDatabases.setCode("SV001"+ maxId.toString());
				}
				else {
					souvenirForDatabases.setCode("SV0001");
				}
				this.souvenirRepo.save(souvenirForDatabases);
				message = "Data Saved! New souvenir has been add with code " + souvenirForDatabases.getCode() + " !";	
			}
			else {
				message = "Nama tersebut sudah ada";
			}		
		}
		return message;
	}

	@Override
	public SouvenirModel getSouvenirFindById(Integer id) {
		return this.souvenirRepo.findById(id).orElse(null);
	}

	@Override
	public String delete(SouvenirModel souvenir) {
		SouvenirModel souvenirForDelete = new SouvenirModel();
		souvenirForDelete = this.souvenirRepo.findById(souvenir.getId()).orElse(null);
		souvenirForDelete.setUpdatedBy("Admin");
		souvenirForDelete.setUpdatedDate(LocalDate.now());
		souvenirForDelete.setIsDelete(true);
		this.souvenirRepo.save(souvenirForDelete);
		return "Data Deleted! Data Employee with Employee ID Number " + souvenirForDelete.getCode()
				+ "has been deleted !";
	}

	@Override
	public List<UnitDTO> getDDLUnit() {
		List<UnitModel> unitList = this.unitRepo.getAllUnit();

		List<UnitDTO> unitDDLList = new ArrayList<UnitDTO>();
		for (UnitModel unit : unitList) {
			UnitDTO unitDto = new UnitDTO();
			unitDto.setId(unit.getId());
			unitDto.setName(unit.getName());
			unitDDLList.add(unitDto);
		}
		return unitDDLList;
	}

}
