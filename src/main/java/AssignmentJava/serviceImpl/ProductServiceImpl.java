package AssignmentJava.serviceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import AssignmentJava.Dto.ProductSearchFormDTO;
import AssignmentJava.helper.ProductSpecifications;
import AssignmentJava.model.ProductModel;
import AssignmentJava.repositories.ProductRepository;
import AssignmentJava.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository productRepo;

	@Override
	public List<Integer> getPageNumbers(Integer totalPages) {
		List<Integer> pageNumbers = IntStream.rangeClosed(1,totalPages).boxed().collect(Collectors.toList());
		return pageNumbers;
	}

	@Override
	public Page<ProductModel> getProductWithPage(Pageable pageable, ProductSearchFormDTO searchForm) {
		Integer pageSize = pageable.getPageSize();
		Integer currentPage = pageable.getPageNumber();
		Integer startItem = currentPage * pageSize;
		List<ProductModel> productList = new ArrayList<ProductModel>();
		if(searchForm.getProductCode()!= null || searchForm.getProductName()!=null || searchForm.getDescription()!=null  || searchForm.getCreatedBy()!=null) {
			if(searchForm.getCreatedDate()!=null) {
				productList = this.productRepo.findAll(Specification
				.where(ProductSpecifications.withProductCode(searchForm.getProductCode()))
				.and(ProductSpecifications.withProductName(searchForm.getProductName()))
				.and(ProductSpecifications.withDescription(searchForm.getDescription()))
				.and(ProductSpecifications.withCreatedDate(searchForm.getCreatedDate()))
				.and(ProductSpecifications.withCreatedBy(searchForm.getCreatedBy()))
				.and(ProductSpecifications.withIsDelete(false)));
			}
			else {
				productList = this.productRepo.findAll(Specification
						.where(ProductSpecifications.withProductCode(searchForm.getProductCode()))
						.and(ProductSpecifications.withProductName(searchForm.getProductName()))
						.and(ProductSpecifications.withDescription(searchForm.getDescription()))
						.and(ProductSpecifications.withCreatedBy(searchForm.getCreatedBy()))
						.and(ProductSpecifications.withIsDelete(false)));
			}
		}
		else {
			productList = this.productRepo.getAllProduct();
		}
        List<ProductModel> list;
 
        if (productList.size() < startItem) {
            list = Collections.emptyList();
        } else {
            Integer toIndex = Math.min(startItem + pageSize, productList.size());
            list = productList.subList(startItem, toIndex);
        }
 
        Page<ProductModel> productPage = new PageImpl<ProductModel>(list, PageRequest.of(currentPage, pageSize), productList.size());
        return productPage;
	}

	@Override
	public String save(ProductModel product) {
		String message = new String();
		ProductModel productForDatabase = new ProductModel();
		if(product.getId()!=null) {
			productForDatabase = this.getProductFindById(product.getId());
			productForDatabase.setCode(product.getCode());
			productForDatabase.setName(product.getName());
			productForDatabase.setDescription(product.getDescription());
			productForDatabase.setUpdatedBy("Admin");
			productForDatabase.setUpdatedDate(LocalDate.now());
			this.productRepo.save(productForDatabase);
			message = "Data Updated! Data Pproduct has been updated";
		}
		else {
			Integer maxId = this.productRepo.getValueId();
			productForDatabase.setCode(product.getCode());
			productForDatabase.setName(product.getName());
			productForDatabase.setDescription(product.getDescription());
			productForDatabase.setCreatedBy("Admin");
			productForDatabase.setCreatedDate(LocalDate.now());
			productForDatabase.setIsDelete(false);
			if(maxId!=null) {
				maxId++;
				productForDatabase.setCode("PR000"+ maxId.toString());
			}
			else {
				productForDatabase.setCode("PR0001");
			}
			this.productRepo.save(productForDatabase);
			message = "Data Saved! New Product has been add with code " + productForDatabase.getCode() + " !";
		}
		return message;
	}

	@Override
	public ProductModel getProductFindById(Integer id) {
		return this.productRepo.findById(id).orElse(null);
	}

	@Override
	public String delete(ProductModel product) {
		ProductModel productForDelete = new ProductModel();
		productForDelete = this.productRepo.findById(product.getId()).orElse(null);
		productForDelete.setUpdatedBy("Admin");
		productForDelete.setUpdatedDate(LocalDate.now());
		productForDelete.setIsDelete(true);
		this.productRepo.save(productForDelete);
		return "Data Deleted! Data Product with code " + productForDelete.getCode() + "has been deleted !";
	}

}
