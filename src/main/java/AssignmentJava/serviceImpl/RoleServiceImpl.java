package AssignmentJava.serviceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import AssignmentJava.Dto.RoleSearchFormDTO;
import AssignmentJava.helper.RoleSpecifications;
import AssignmentJava.model.RoleModel;
import AssignmentJava.repositories.RoleRepository;
import AssignmentJava.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleRepository roleRepo;
	
	@Override
	public List<String> getAllRoleName() {
		return roleRepo.getAllRoleName();
	}

	@Override
	public List<String> getAllRoleCode() {
		return roleRepo.getAllRoleCode();
	}

	@Override
	public List<Integer> getPageNumbers(Integer totalPages) {
		List<Integer> pageNumbers = IntStream.rangeClosed(1,totalPages).boxed().collect(Collectors.toList());
		return pageNumbers;
	}

	@Override
	public Page<RoleModel> getRoleWithPage(Pageable pageable, RoleSearchFormDTO searchForm) {
		Integer pageSize = pageable.getPageSize();
		Integer currentPage = pageable.getPageNumber();
		Integer startItem = currentPage * pageSize;
		List<RoleModel> roleList = new ArrayList<RoleModel>();
		if(searchForm.getRoleCode()!= null || searchForm.getRoleName()!=null || searchForm.getCreatedBy()!=null) {
			if(searchForm.getCreatedDate()!=null) {
				roleList = this.roleRepo.findAll(Specification
				.where(RoleSpecifications.withRoleCode(searchForm.getRoleCode()))
				.and(RoleSpecifications.withRoleName(searchForm.getRoleName()))
				.and(RoleSpecifications.withCreatedDate(searchForm.getCreatedDate()))
				.and(RoleSpecifications.withCreatedBy(searchForm.getCreatedBy()))
				.and(RoleSpecifications.withIsDelete(false)));
			}
			else {
				roleList = this.roleRepo.findAll(Specification
						.where(RoleSpecifications.withRoleCode(searchForm.getRoleCode()))
						.and(RoleSpecifications.withRoleName(searchForm.getRoleName()))
						.and(RoleSpecifications.withCreatedBy(searchForm.getCreatedBy()))
						.and(RoleSpecifications.withIsDelete(false)));
			}
		}
		else {
			roleList = this.roleRepo.getAllRole();
		}
        List<RoleModel> list;
 
        if (roleList.size() < startItem) {
            list = Collections.emptyList();
        } else {
            Integer toIndex = Math.min(startItem + pageSize, roleList.size());
            list = roleList.subList(startItem, toIndex);
        }
 
        Page<RoleModel> rolePage = new PageImpl<RoleModel>(list, PageRequest.of(currentPage, pageSize), roleList.size());
        return rolePage;
	}

	@Override
	public String save(RoleModel role) {
		String message = new String();
		RoleModel roleForDatabase = new RoleModel();
		roleForDatabase = this.roleRepo.findByRoleName(role.getName());
		
		if(role.getId()!=null) {
			roleForDatabase = this.getRoleFindById(role.getId());
			roleForDatabase.setName(role.getName());
			roleForDatabase.setDescription(role.getDescription());
			roleForDatabase.setUpdatedBy("Admin");
			roleForDatabase.setUpdatedDate(LocalDate.now());
			this.roleRepo.save(roleForDatabase);
			message = "Data Updated! Data Role has been updated";
		}
		
		else {
			if(roleForDatabase==null) {
				RoleModel roleForDatabases = new RoleModel();
				Integer maxId = this.roleRepo.getValueId();
				roleForDatabases.setName(role.getName());
				roleForDatabases.setDescription(role.getDescription());
				roleForDatabases.setCreatedBy("Admin");
				roleForDatabases.setCreatedDate(LocalDate.now());
				roleForDatabases.setIsDelete(false);
				if(maxId!=null && maxId<=8) {
					maxId++;
					roleForDatabases.setCode("RO000"+ maxId.toString());
				}
				else if(maxId!=null && maxId>=9) {
					maxId++;
					roleForDatabases.setCode("RO001"+ maxId.toString());
				}
				else {
					roleForDatabases.setCode("RO0001");
				}
				this.roleRepo.save(roleForDatabases);
				message = "Data Saved! New role has been add with code " + roleForDatabases.getCode() + " !";	
			}
			else {
				message = "Nama tersebut sudah ada";
			}		
		}
		return message;
	}

	@Override
	public RoleModel getRoleFindById(Integer id) {
		return this.roleRepo.findById(id).orElse(null);
	}

	@Override
	public String delete(RoleModel role) {
		RoleModel roleForDelete = new RoleModel();
		roleForDelete = this.roleRepo.findById(role.getId()).orElse(null);
		roleForDelete.setUpdatedBy("Admin");
		roleForDelete.setUpdatedDate(LocalDate.now());
		roleForDelete.setIsDelete(true);
		this.roleRepo.save(roleForDelete);
		return "Data Deleted! Data role with code " + roleForDelete.getCode() + "has been deleted !";
	}

}
