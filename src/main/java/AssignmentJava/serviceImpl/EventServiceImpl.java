package AssignmentJava.serviceImpl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import AssignmentJava.Dto.EmployeeDTO;
import AssignmentJava.Dto.EventSearchFormDTO;
import AssignmentJava.helper.EventSpecifications;
import AssignmentJava.model.EmployeeModel;
import AssignmentJava.model.EventModel;
import AssignmentJava.repositories.EmployeeRepository;
import AssignmentJava.repositories.EventRepository;
import AssignmentJava.service.EventService;

@Service
public class EventServiceImpl implements EventService {

	@Autowired
	private EventRepository eventRepo;
	
	@Autowired
	private EmployeeRepository employeeRepo;
	
	@Override
	public List<Integer> getPageNumbers(Integer totalPages) {
		List<Integer> pageNumbers = IntStream.rangeClosed(1,totalPages).boxed().collect(Collectors.toList());
		return pageNumbers;
	}

	@Override
	public Page<EventModel> getEventWithPage(Pageable pageable, EventSearchFormDTO searchForm) {
		Integer pageSize = pageable.getPageSize();
		Integer currentPage = pageable.getPageNumber();
		Integer startItem = currentPage * pageSize;
		List<EventModel> eventList = new ArrayList<EventModel>();
		if(searchForm.getTransactionCode()!= null || searchForm.getRequest()!=null || searchForm.getRequestDate()!=null || searchForm.getCreatedDate()!=null  || searchForm.getCreatedBy()!=null) {
			if(searchForm.getTransactionCode()!=null && searchForm.getRequest()==null && searchForm.getRequestDate()==null && searchForm.getCreatedDate()==null && searchForm.getCreatedBy()==null){
				eventList = this.eventRepo.findAll(Specification
				.where(EventSpecifications.withTransactionCode(searchForm.getTransactionCode()))
				.and(EventSpecifications.withEventIsDelete(false)));
			}
			else if(searchForm.getRequest()!=null || searchForm.getTransactionCode()==null && searchForm.getRequestDate()==null && searchForm.getCreatedDate()==null && searchForm.getCreatedBy()==null){
				eventList = this.eventRepo.findAll(Specification
				.where(EventSpecifications.withRequestEvent(searchForm.getRequest()))
				.and(EventSpecifications.withEventIsDelete(false)));			
			}
			else if(searchForm.getCreatedBy()!=null && searchForm.getCreatedBy()!="" || searchForm.getTransactionCode()==null && searchForm.getRequest()==null && searchForm.getRequestDate()==null && searchForm.getCreatedDate()==null) {
				eventList = this.eventRepo.findAll(Specification
				.where(EventSpecifications.withEventCreatedBy(searchForm.getCreatedBy()))
				.and(EventSpecifications.withEventIsDelete(false)));			
			}
			
			else if(searchForm.getEvent()!=null) {
				searchForm.setEvent(searchForm.getEvent().toUpperCase());
				
				if(searchForm.getEvent().equals("SUBMITED")) {
					eventList = this.eventRepo.findAll(Specification
					.where(EventSpecifications.withStatus(1))
					.and(EventSpecifications.withEventIsDelete(false)));
				}
				else if(searchForm.getEvent().equals("IN PROGRESS")) {
					eventList = this.eventRepo.findAll(Specification
					.where(EventSpecifications.withStatus(2))
					.and(EventSpecifications.withEventIsDelete(false)));
				}
				else if(searchForm.getEvent().equals("DONE")) {
					eventList = this.eventRepo.findAll(Specification
					.where(EventSpecifications.withStatus(3))
					.and(EventSpecifications.withEventIsDelete(false)));
				}
				else if(searchForm.getEvent().equals("REJECTED")) {
					eventList = this.eventRepo.findAll(Specification
					.where(EventSpecifications.withStatus(0))
					.and(EventSpecifications.withEventIsDelete(false)));
				}
			}
			
			else if(searchForm.getCreatedDate()!=null && searchForm.getRequestDate()!=null) {
				eventList = this.eventRepo.findAll(Specification
				.where(EventSpecifications.withEventCreatedDate(searchForm.getCreatedDate()))
				.and(EventSpecifications.withRequestDate(searchForm.getRequestDate()))
				.and(EventSpecifications.withEventIsDelete(false)));
			}
			
			else if(searchForm.getCreatedDate()!=null) {
				eventList = this.eventRepo.findAll(Specification
				.where(EventSpecifications.withEventCreatedDate(searchForm.getCreatedDate()))
				.and(EventSpecifications.withEventIsDelete(false)));
			}
			else if(searchForm.getRequestDate()!=null) {
				eventList = this.eventRepo.findAll(Specification
				.where(EventSpecifications.withRequestDate(searchForm.getRequestDate()))
				.and(EventSpecifications.withEventIsDelete(false)));
			}
		}
		else {
			eventList = this.eventRepo.getAllEvent();
		}
        List<EventModel> list;
 
        if (eventList.size() < startItem) {
            list = Collections.emptyList();
        } else {
            Integer toIndex = Math.min(startItem + pageSize, eventList.size());
            list = eventList.subList(startItem, toIndex);
        }
 
        Page<EventModel> productPage = new PageImpl<EventModel>(list, PageRequest.of(currentPage, pageSize), eventList.size());
        return productPage;
	}

	@Override
	public String save(EventModel event) {
		String message = new String();
		EventModel eventForDatabase = new EventModel();
		
		if(event.getId()==null)	{ //create
			Integer maxId = this.eventRepo.getValueId();
			eventForDatabase.setCode(event.getCode());
			eventForDatabase.setEventName(event.getEventName());
			eventForDatabase.setPlace(event.getPlace());
			eventForDatabase.setStartDate(event.getStartDate());
			eventForDatabase.setEndDate(event.getEndDate());
			eventForDatabase.setBudget(event.getBudget());
			eventForDatabase.setRequestBy(1);
			eventForDatabase.setRequestDate(LocalDate.now());
			eventForDatabase.setNote(event.getNote());
			eventForDatabase.setStatus(1);
			eventForDatabase.setCreatedBy("Admin");
			eventForDatabase.setCreatedDate(LocalDate.now());
			eventForDatabase.setIsDelete(false);
			if(maxId==null) {
				eventForDatabase.setCode("TRWOEV" + LocalDate.now().format(DateTimeFormatter.ofPattern("ddMMyy")) +"00001");
			}
			else if(maxId!=null && maxId<=8) {
				maxId++;
				eventForDatabase.setCode("TRWOEV"+ LocalDate.now().format(DateTimeFormatter.ofPattern("ddMMyy")) + "0000"+ maxId.toString());
			}
			else if(maxId!=null && maxId>=9) {
				maxId++;
				eventForDatabase.setCode("TRWOEV"+ LocalDate.now().format(DateTimeFormatter.ofPattern("ddMMyy")) + "000"+ maxId.toString());
			}
			this.eventRepo.save(eventForDatabase);
			message = "Data Saved! Transaction event request has been add with code " + eventForDatabase.getCode() + " !";
		}
		
		else if(event.getId()!=null && event.getStatus()==null && event.getAssignTo()==null) { //approved1
			eventForDatabase = this.getEventFindById(event.getId());
			eventForDatabase.setStatus(2);
			eventForDatabase.setApprovedDate(LocalDate.now());
			
			this.eventRepo.save(eventForDatabase);
			message = "Data Approved! Transaction event request with code " + eventForDatabase.getCode() + " Has been Approved !";
			
		}
		
		else if(event.getId()!=null && event.getAssignTo()!=null) { //approved2
			eventForDatabase = this.getEventFindById(event.getId());
			eventForDatabase.setStatus(2);
			eventForDatabase.setApprovedBy(1);
			eventForDatabase.setApprovedDate(LocalDate.now());
			eventForDatabase.setAssignTo(event.getAssignTo());
			
			this.eventRepo.save(eventForDatabase);
			message = "Data Approved! Transaction event request with code " + eventForDatabase.getCode() + " Has been Approved !";
			
		}
		
		else if(event.getId()!=null && event.getRejectReason()!=null) {	//rejected
			eventForDatabase = this.getEventFindById(event.getId());
			eventForDatabase.setStatus(0);
			eventForDatabase.setApprovedDate(LocalDate.now());
			eventForDatabase.setRejectReason(event.getRejectReason());
			eventForDatabase.setAssignTo(1);
			
			this.eventRepo.save(eventForDatabase);
			message = "Data Rejected! Transaction event request with code " + eventForDatabase.getCode() + " is rejected by Administrator!";
			
		}
		
		else if(event.getId()!=null && event.getStatus()==1) { //edit
			eventForDatabase = this.getEventFindById(event.getId());
			eventForDatabase.setEventName(event.getEventName());
			eventForDatabase.setPlace(event.getPlace());
			eventForDatabase.setStartDate(event.getStartDate());
			eventForDatabase.setEndDate(event.getEndDate());
			eventForDatabase.setBudget(event.getBudget());
			eventForDatabase.setNote(event.getNote());
			eventForDatabase.setStatus(1);
			eventForDatabase.setUpdatedBy("Admin");
			eventForDatabase.setUpdatedDate(LocalDate.now());
			this.eventRepo.save(eventForDatabase);
			message = "Data Updated! Transaction event request with code " + eventForDatabase.getCode() + " has been updated !";
		}
		
		else if(event.getStatus()==0 || event.getStatus()==2) {
			eventForDatabase = this.getEventFindById(event.getId());
			eventForDatabase.setStatus(3);
			eventForDatabase.setClosedDate(LocalDate.now());
			
			this.eventRepo.save(eventForDatabase);
			message = "Data Closed! Transaction event request with code " + eventForDatabase.getCode() + " has been closed request !";
			
		}
		return message;
	}

	@Override
	public EventModel getEventFindById(Integer id) {
		return this.eventRepo.findById(id).orElse(null);
	}

	@Override
	public String delete(EventModel event) {
		EventModel eventForDelete = new EventModel();
		eventForDelete = this.eventRepo.findById(event.getId()).orElse(null);
		eventForDelete.setUpdatedBy("Admin");
		eventForDelete.setUpdatedDate(LocalDate.now());
		eventForDelete.setIsDelete(true);
		this.eventRepo.save(eventForDelete);
		return "Data Deleted! Data Product with code " + eventForDelete.getCode() + "has been deleted !";
	}

	@Override
	public List<EmployeeDTO> getDDLEmployee() {
		List<EmployeeModel> employeeList = this.employeeRepo.getAllEmployee();
		
		List<EmployeeDTO> employeeDDLList = new ArrayList<EmployeeDTO>();
		for (EmployeeModel employee : employeeList) {
			EmployeeDTO employeeDto = new EmployeeDTO();
			employeeDto.setId(employee.getId());
			employeeDto.setFirstName(employee.getFirstName());
			employeeDto.setLastName(employee.getLastName());
			employeeDDLList.add(employeeDto);
		}
		return employeeDDLList;
	}

	@Override
	public String rejected(EventModel event) {
		String message = new String();
		EventModel eventForDatabase = new EventModel();
		
		eventForDatabase.setCode(event.getCode());
		eventForDatabase.setEventName(event.getEventName());
		eventForDatabase.setPlace(event.getPlace());
		eventForDatabase.setStartDate(event.getStartDate());
		eventForDatabase.setEndDate(event.getEndDate());
		eventForDatabase.setBudget(event.getBudget());
		eventForDatabase.setRequestBy(1);
		eventForDatabase.setRequestDate(LocalDate.now());
		eventForDatabase.setNote(event.getNote());
		eventForDatabase.setStatus(0);
		eventForDatabase.setCreatedBy("Admin");
		eventForDatabase.setCreatedDate(LocalDate.now());
		eventForDatabase.setIsDelete(false);
		eventForDatabase.setAssignTo(event.getAssignTo());
		eventForDatabase.setRejectReason(event.getRejectReason());
		
		this.eventRepo.save(eventForDatabase);
		message = "Data Rejected! Transaction event request with code " + eventForDatabase.getCode() + " is rejected by Administrator!";
		
		return message;
	}

}
