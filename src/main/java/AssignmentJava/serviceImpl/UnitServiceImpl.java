package AssignmentJava.serviceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import AssignmentJava.Dto.UnitSearchFormDTO;
import AssignmentJava.helper.UnitSpecifications;
import AssignmentJava.model.UnitModel;
import AssignmentJava.repositories.UnitRepository;
import AssignmentJava.service.UnitService;

@Service
public class UnitServiceImpl implements UnitService {

	@Autowired
	private UnitRepository unitRepo;
	
	@Override
	public List<String> getAllUnitName() {
		return unitRepo.getAllUnitName();
	}

	@Override
	public List<String> getAllUnitCode() {
		return unitRepo.getAllUnitCode();
	}

	@Override
	public List<Integer> getPageNumbers(Integer totalPages) {
		List<Integer> pageNumbers = IntStream.rangeClosed(1,totalPages).boxed().collect(Collectors.toList());
		return pageNumbers;
	}

	@Override
	public Page<UnitModel> getUnitWithPage(Pageable pageable, UnitSearchFormDTO searchForm) {
		Integer pageSize = pageable.getPageSize();
		Integer currentPage = pageable.getPageNumber();
		Integer startItem = currentPage * pageSize;
		List<UnitModel> unitList = new ArrayList<UnitModel>();
		if(searchForm.getUnitCode()!= null || searchForm.getUnitName()!=null || searchForm.getCreatedBy()!=null) {
			if(searchForm.getCreatedDate()!=null) {
				unitList = this.unitRepo.findAll(Specification
				.where(UnitSpecifications.withUnitCode(searchForm.getUnitCode()))
				.and(UnitSpecifications.withUnitName(searchForm.getUnitName()))
				.and(UnitSpecifications.withCreatedDate(searchForm.getCreatedDate()))
				.and(UnitSpecifications.withCreatedBy(searchForm.getCreatedBy()))
				.and(UnitSpecifications.withIsDelete(false)));
			}
			else {
				unitList = this.unitRepo.findAll(Specification
						.where(UnitSpecifications.withUnitCode(searchForm.getUnitCode()))
						.and(UnitSpecifications.withUnitName(searchForm.getUnitName()))
						.and(UnitSpecifications.withCreatedBy(searchForm.getCreatedBy()))
						.and(UnitSpecifications.withIsDelete(false)));
			}
		}
		else {
			unitList = this.unitRepo.getAllUnit();
		}
        List<UnitModel> list;
 
        if (unitList.size() < startItem) {
            list = Collections.emptyList();
        } else {
            Integer toIndex = Math.min(startItem + pageSize, unitList.size());
            list = unitList.subList(startItem, toIndex);
        }
 
        Page<UnitModel> unitPage = new PageImpl<UnitModel>(list, PageRequest.of(currentPage, pageSize), unitList.size());
        return unitPage;
	}

	@Override
	public String save(UnitModel unit) {
		String message = new String();
		UnitModel unitForDatabase = new UnitModel();
		unitForDatabase = this.unitRepo.findByUnitName(unit.getName());
		
		if(unit.getId()!=null) {
			unitForDatabase = this.getUnitFindById(unit.getId());
			unitForDatabase.setName(unit.getName());
			unitForDatabase.setDescription(unit.getDescription());
			unitForDatabase.setUpdatedBy("Admin");
			unitForDatabase.setUpdatedDate(LocalDate.now());
			this.unitRepo.save(unitForDatabase);
			message = "Data Updated! Data Unit has been updated";
		}
		
		else {
			if(unitForDatabase==null) {
				UnitModel unitForDatabases = new UnitModel();
				Integer maxId = this.unitRepo.getValueId();
				unitForDatabases.setName(unit.getName());
				unitForDatabases.setDescription(unit.getDescription());
				unitForDatabases.setCreatedBy("Admin");
				unitForDatabases.setCreatedDate(LocalDate.now());
				unitForDatabases.setIsDelete(false);
				if(maxId!=null && maxId<=8) {
					maxId++;
					unitForDatabases.setCode("UN00"+ maxId.toString());
				}
				else if(maxId!=null && maxId>=9) {
					maxId++;
					unitForDatabases.setCode("UN01"+ maxId.toString());
				}
				else {
					unitForDatabases.setCode("UN001");
				}
				this.unitRepo.save(unitForDatabases);
				message = "Data Saved! New role has been add with code " + unitForDatabases.getCode() + " !";	
			}
			else {
				message = "Nama tersebut sudah ada";
			}		
		}
		return message;
	}

	@Override
	public UnitModel getUnitFindById(Integer id) {
		return this.unitRepo.findById(id).orElse(null);
	}

	@Override
	public String delete(UnitModel unit) {
		UnitModel unitForDelete = new UnitModel();
		unitForDelete = this.unitRepo.findById(unit.getId()).orElse(null);
		unitForDelete.setUpdatedBy("Admin");
		unitForDelete.setUpdatedDate(LocalDate.now());
		unitForDelete.setIsDelete(true);
		this.unitRepo.save(unitForDelete);
		return "Data Deleted! Data unit with code " + unitForDelete.getCode() + "has been deleted !";
	}

}
