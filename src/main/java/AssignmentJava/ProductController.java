package AssignmentJava;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import AssignmentJava.Dto.ProductSearchFormDTO;
import AssignmentJava.model.ProductModel;
import AssignmentJava.service.ProductService;

@Controller
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@GetMapping("index")
	public ModelAndView index(@RequestParam("page") Optional<Integer> page, @ModelAttribute ProductSearchFormDTO searchForm, HttpSession session) {
		
		ModelAndView view = new ModelAndView("/product/index");
		
        Integer currentPage = page.orElse(1);
		Page<ProductModel> productPage = this.productService.getProductWithPage(PageRequest.of(currentPage - 1, 2), searchForm);
		
		Integer totalPages = productPage.getTotalPages();
		if(totalPages>0) {
			List<Integer> pageNumbers = this.productService.getPageNumbers(totalPages);
			view.addObject("pageNumbers", pageNumbers);
		}
		String message= null;
		if (session.getAttribute("message") != null) {
	    	message = (String) session.getAttribute("message");
	    }
		ProductSearchFormDTO searchFormDTO = searchForm;
		view.addObject("searchForm", searchFormDTO);
		view.addObject("productPage", productPage);
		view.addObject("message", message);
		session.removeAttribute("message");
		return view;
	}
	
	@GetMapping("add")
	public ModelAndView add() {
		ModelAndView view = new ModelAndView("/product/add");
		ProductModel product = new ProductModel();
		view.addObject("product", product);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute ProductModel product, BindingResult result, HttpSession session) {
		if(!result.hasErrors()) {
			String message = this.productService.save(product);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/product/index");
		}
		else {
			return new ModelAndView("redirect:/product/add");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id")Integer id) {
		ModelAndView view = new ModelAndView("/product/edit");
		ProductModel product = this.productService.getProductFindById(id);
		String title = "Edit Product - " + product.getName() + "(" + product.getCode() + ")";
		view.addObject("product",product);
		view.addObject("title",title);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id")Integer id) {
		ModelAndView view = new ModelAndView("/product/delete");
		ProductModel product = new ProductModel();
		product.setId(id);
		view.addObject("product",product);
		return view;
	}
	
	@PostMapping("deletedata")
	public ModelAndView deletedata(@ModelAttribute ProductModel product, BindingResult result, HttpSession session) {
		if(!result.hasErrors()) {
			String message = this.productService.delete(product);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/product/index");
		}
		else {
			return new ModelAndView("redirect:/product/delete");
		}
	}
	
	@GetMapping("details/{id}")
	public ModelAndView details(@PathVariable("id")Integer id) {
		ModelAndView view = new ModelAndView("/product/details");
		ProductModel product = this.productService.getProductFindById(id);
		String title = "View Product - " + product.getName() + "(" + product.getCode() + ")";
		view.addObject("product",product);
		view.addObject("title",title);
		return view;
	}
	
}
