package AssignmentJava.helper;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.Specification;

import AssignmentJava.model.CompanyModel;

public class CompanySpecifications {

	public static Specification<CompanyModel> withCompanyName(String name){
        return (root, query, builder) -> builder.like(root.get("name"), "%" + name + "%");
	}
	public static Specification<CompanyModel> withCompanyCode(String code){
        return (root, query, builder) -> builder.like(root.get("code"), "%" + code + "%");
	}
	public static Specification<CompanyModel> withCreatedBy(String createdBy){
        return (root, query, builder) -> builder.like(root.get("createdBy"), "%" + createdBy + "%");
	}
	public static Specification<CompanyModel> withCreatedDate(LocalDate createdDate){
        return (root, query, builder) -> builder.equal(root.get("createdDate"), createdDate);
	}
	public static Specification<CompanyModel> withIsDelete(Boolean isDelete){
        return (root, query, builder) -> builder.equal(root.get("isDelete"), isDelete);
	}
	
}
