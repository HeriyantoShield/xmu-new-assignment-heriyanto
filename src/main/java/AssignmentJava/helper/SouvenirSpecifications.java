package AssignmentJava.helper;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.Specification;

import AssignmentJava.model.SouvenirModel;

public class SouvenirSpecifications {
	public static Specification<SouvenirModel> withSouvenirName(String name){
        return (root, query, builder) -> builder.like(root.get("name"), "%" + name + "%");
	}
	public static Specification<SouvenirModel> withSouvenirCode(String code){
        return (root, query, builder) -> builder.like(root.get("code"), "%" + code + "%");
	}
	public static Specification<SouvenirModel> withSouvenirUnit(Integer souvenirName) {
		return (root, query, builder) -> builder.equal(root.get("unit").get("id"), souvenirName);
	}
	public static Specification<SouvenirModel> withCreatedBy(String createdBy){
        return (root, query, builder) -> builder.like(root.get("createdBy"), "%" + createdBy + "%");
	}
	public static Specification<SouvenirModel> withCreatedDate(LocalDate createdDate){
        return (root, query, builder) -> builder.equal(root.get("createdDate"), createdDate);
	}
	public static Specification<SouvenirModel> withIsDelete(Boolean isDelete){
        return (root, query, builder) -> builder.equal(root.get("isDelete"), isDelete);
	}
}
