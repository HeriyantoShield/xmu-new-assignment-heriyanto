package AssignmentJava.helper;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.Specification;

import AssignmentJava.model.UnitModel;

public class UnitSpecifications {

	public static Specification<UnitModel> withUnitName(String name){
        return (root, query, builder) -> builder.like(root.get("name"), "%" + name + "%");
	}
	public static Specification<UnitModel> withUnitCode(String code){
        return (root, query, builder) -> builder.like(root.get("code"), "%" + code + "%");
	}
	public static Specification<UnitModel> withCreatedBy(String createdBy){
        return (root, query, builder) -> builder.like(root.get("createdBy"), "%" + createdBy + "%");
	}
	public static Specification<UnitModel> withCreatedDate(LocalDate createdDate){
        return (root, query, builder) -> builder.equal(root.get("createdDate"), createdDate);
	}
	public static Specification<UnitModel> withIsDelete(Boolean isDelete){
        return (root, query, builder) -> builder.equal(root.get("isDelete"), isDelete);
	}
	
}
