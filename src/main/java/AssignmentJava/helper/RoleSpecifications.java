package AssignmentJava.helper;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.Specification;

import AssignmentJava.model.RoleModel;

public class RoleSpecifications {

	public static Specification<RoleModel> withRoleName(String name){
        return (root, query, builder) -> builder.like(root.get("name"), "%" + name + "%");
	}
	public static Specification<RoleModel> withRoleCode(String code){
        return (root, query, builder) -> builder.like(root.get("code"), "%" + code + "%");
	}
	public static Specification<RoleModel> withCreatedBy(String createdBy){
        return (root, query, builder) -> builder.like(root.get("createdBy"), "%" + createdBy + "%");
	}
	public static Specification<RoleModel> withCreatedDate(LocalDate createdDate){
        return (root, query, builder) -> builder.equal(root.get("createdDate"), createdDate);
	}
	public static Specification<RoleModel> withIsDelete(Boolean isDelete){
        return (root, query, builder) -> builder.equal(root.get("isDelete"), isDelete);
	}
	
}
