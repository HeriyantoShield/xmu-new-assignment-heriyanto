package AssignmentJava.helper;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.Specification;

import AssignmentJava.model.ProductModel;

public class ProductSpecifications {

	public static Specification<ProductModel> withProductName(String name){
        return (root, query, builder) -> builder.like(root.get("name"), "%" + name + "%");
	}
	public static Specification<ProductModel> withProductCode(String code){
        return (root, query, builder) -> builder.like(root.get("code"), "%" + code + "%");
	}
	public static Specification<ProductModel> withDescription(String description){
        return (root, query, builder) -> builder.like(root.get("description"), "%" + description + "%");
	}
	public static Specification<ProductModel> withCreatedBy(String createdBy){
        return (root, query, builder) -> builder.like(root.get("createdBy"), "%" + createdBy + "%");
	}
	public static Specification<ProductModel> withCreatedDate(LocalDate createdDate){
        return (root, query, builder) -> builder.equal(root.get("createdDate"), createdDate);
	}
	public static Specification<ProductModel> withIsDelete(Boolean isDelete){
        return (root, query, builder) -> builder.equal(root.get("isDelete"), isDelete);
	}
	
}
