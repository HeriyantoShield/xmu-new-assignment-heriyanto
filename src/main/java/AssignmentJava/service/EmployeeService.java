package AssignmentJava.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import AssignmentJava.Dto.CompanyDTO;
import AssignmentJava.Dto.EmployeeSearchFormDTO;

import AssignmentJava.model.EmployeeModel;

public interface EmployeeService {

	List<Integer> getPageNumbers(Integer totalPages);

	Page<EmployeeModel> getEmployeeWithPage(Pageable pageable, EmployeeSearchFormDTO searchForm);

	String save(EmployeeModel employee);

	EmployeeModel getEmployeeFindById(Integer id);

	String delete(EmployeeModel employee);

	List<CompanyDTO> getDDLCompany();
	
}
