package AssignmentJava.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import AssignmentJava.Dto.ProductSearchFormDTO;
import AssignmentJava.model.ProductModel;

public interface ProductService {

	List<Integer> getPageNumbers(Integer totalPages);
	Page<ProductModel> getProductWithPage(Pageable pageable, ProductSearchFormDTO searchForm);
	String save(ProductModel product);
	ProductModel getProductFindById(Integer id);
	String delete(ProductModel product);
	
}
