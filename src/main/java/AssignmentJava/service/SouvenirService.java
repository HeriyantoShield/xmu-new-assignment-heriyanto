package AssignmentJava.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import AssignmentJava.Dto.SouvenirSearchFormDTO;
import AssignmentJava.Dto.UnitDTO;
import AssignmentJava.model.SouvenirModel;

public interface SouvenirService {

	List<String> getAllSouvenirName();
	List<String> getAllSouvenirCode();
	List<Integer> getPageNumbers(Integer totalPages);
	Page<SouvenirModel> getSouvenirWithPage(Pageable pageable, SouvenirSearchFormDTO searchForm);
	String save(SouvenirModel souvenir);
	SouvenirModel getSouvenirFindById(Integer id);
	String delete(SouvenirModel souvenir);
	List<UnitDTO> getDDLUnit();
	
}
