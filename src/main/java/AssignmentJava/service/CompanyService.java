package AssignmentJava.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import AssignmentJava.Dto.CompanySearchFormDTO;
import AssignmentJava.model.CompanyModel;

public interface CompanyService {

	List<String> getAllCompanyName();
	List<String> getAllCompanyCode();
	List<Integer> getPageNumbers(Integer totalPages);
	Page<CompanyModel> getCompanyWithPage(Pageable pageable, CompanySearchFormDTO searchForm);
	String save(CompanyModel company);
	CompanyModel getCompanyFindById(Integer id);
	String delete(CompanyModel company);
	
}
