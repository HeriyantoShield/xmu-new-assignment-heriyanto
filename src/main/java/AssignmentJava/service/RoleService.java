package AssignmentJava.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import AssignmentJava.Dto.RoleSearchFormDTO;
import AssignmentJava.model.RoleModel;

public interface RoleService {

	List<String> getAllRoleName();
	List<String> getAllRoleCode();
	List<Integer> getPageNumbers(Integer totalPages);
	Page<RoleModel> getRoleWithPage(Pageable pageable, RoleSearchFormDTO searchForm);
	String save(RoleModel role);
	RoleModel getRoleFindById(Integer id);
	String delete(RoleModel role);
	
}
