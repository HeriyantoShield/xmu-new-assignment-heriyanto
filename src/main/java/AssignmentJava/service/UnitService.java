package AssignmentJava.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import AssignmentJava.Dto.UnitSearchFormDTO;
import AssignmentJava.model.UnitModel;

public interface UnitService {

	List<String> getAllUnitName();
	List<String> getAllUnitCode();
	List<Integer> getPageNumbers(Integer totalPages);
	Page<UnitModel> getUnitWithPage(Pageable pageable, UnitSearchFormDTO searchForm);
	String save(UnitModel unit);
	UnitModel getUnitFindById(Integer id);
	String delete(UnitModel unit);
	
}
