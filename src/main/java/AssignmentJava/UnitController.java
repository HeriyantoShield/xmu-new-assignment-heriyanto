package AssignmentJava;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import AssignmentJava.Dto.UnitSearchFormDTO;
import AssignmentJava.model.UnitModel;
import AssignmentJava.service.UnitService;

@Controller
@RequestMapping("/unit")
public class UnitController {

	@Autowired
	private UnitService unitService;
	
	@GetMapping("index")
	public ModelAndView index(@RequestParam("page") Optional<Integer> page, @ModelAttribute UnitSearchFormDTO searchForm, HttpSession session) {
		
		ModelAndView view = new ModelAndView("/unit/index");
		List<String> unitNameList = this.unitService.getAllUnitName();
		List<String> unitCodeList = this.unitService.getAllUnitCode();
		
        Integer currentPage = page.orElse(1);
		Page<UnitModel> unitPage = this.unitService.getUnitWithPage(PageRequest.of(currentPage - 1, 2), searchForm);
		
		Integer totalPages = unitPage.getTotalPages();
		if(totalPages>0) {
			List<Integer> pageNumbers = this.unitService.getPageNumbers(totalPages);
			view.addObject("pageNumbers", pageNumbers);
		}
		String message= null;
		if (session.getAttribute("message") != null) {
	    	message = (String) session.getAttribute("message");
	    }
		UnitSearchFormDTO searchFormDTO = searchForm;
		view.addObject("searchForm", searchFormDTO);
		view.addObject("unitNameList", unitNameList);
		view.addObject("unitCodeList", unitCodeList);
		view.addObject("unitPage", unitPage);
		view.addObject("message", message);
		session.removeAttribute("message");
		return view;
	}
	
	@GetMapping("add")
	public ModelAndView add() {
		ModelAndView view = new ModelAndView("/unit/add");
		UnitModel unit = new UnitModel();
		view.addObject("unit", unit);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute UnitModel unit, BindingResult result, HttpSession session) {
		if(!result.hasErrors()) {
			String message = this.unitService.save(unit);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/unit/index");
		}
		else {
			return new ModelAndView("redirect:/unit/add");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id")Integer id) {
		ModelAndView view = new ModelAndView("/unit/edit");
		UnitModel unit = this.unitService.getUnitFindById(id);
		String title = "Edit Unit - " + unit.getName() + "(" + unit.getCode() + ")";
		view.addObject("unit",unit);
		view.addObject("title",title);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id")Integer id) {
		ModelAndView view = new ModelAndView("/unit/delete");
		UnitModel unit = new UnitModel();
		unit.setId(id);
		view.addObject("unit",unit);
		return view;
	}
	
	@PostMapping("deletedata")
	public ModelAndView deletedata(@ModelAttribute UnitModel unit, BindingResult result, HttpSession session) {
		if(!result.hasErrors()) {
			String message = this.unitService.delete(unit);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/unit/index");
		}
		else {
			return new ModelAndView("redirect:/unit/delete");
		}
	}
	
	@GetMapping("details/{id}")
	public ModelAndView details(@PathVariable("id")Integer id) {
		ModelAndView view = new ModelAndView("/unit/details");
		UnitModel unit = this.unitService.getUnitFindById(id);
		String title = "View Unit - " + unit.getName() + "(" + unit.getCode() + ")";
		view.addObject("unit",unit);
		view.addObject("title",title);
		return view;
	}
	
}
