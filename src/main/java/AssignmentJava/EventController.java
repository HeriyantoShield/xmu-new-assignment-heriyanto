package AssignmentJava;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import AssignmentJava.Dto.EmployeeDTO;
import AssignmentJava.Dto.EventSearchFormDTO;
import AssignmentJava.model.EventModel;
import AssignmentJava.service.EventService;

@Controller
@RequestMapping("/event")
public class EventController {

	@Autowired
	private EventService eventService;
	
	@GetMapping("index")
	public ModelAndView index(@RequestParam("page") Optional<Integer> page, @ModelAttribute EventSearchFormDTO searchForm, HttpSession session) {
		ModelAndView view = new ModelAndView("/event/index");
		
        Integer currentPage = page.orElse(1);
		Page<EventModel> eventPage = this.eventService.getEventWithPage(PageRequest.of(currentPage - 1, 2), searchForm);
		
		Integer totalPages = eventPage.getTotalPages();
		if(totalPages>0) {
			List<Integer> pageNumbers = this.eventService.getPageNumbers(totalPages);
			view.addObject("pageNumbers", pageNumbers);
		}
		String message= null;
		if (session.getAttribute("message") != null) {
	    	message = (String) session.getAttribute("message");
	    }
		
		EventSearchFormDTO searchFormDTO = searchForm;
		view.addObject("searchForm", searchFormDTO);
		view.addObject("eventPage", eventPage);
		view.addObject("message", message);
		session.removeAttribute("message");
		return view;
	}
	
	@GetMapping("add")
	public ModelAndView add() {
		ModelAndView view = new ModelAndView("/event/add");
		EventModel event = new EventModel();
		view.addObject("event", event);
		return view;
	}

	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute EventModel event, BindingResult result, HttpSession session) {
		
		if(event.getRejectReason()!=null) {
			EventModel events = this.eventService.getEventFindById(event.getId());
			events.setRejectReason(event.getRejectReason());
			String message = this.eventService.save(events);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/event/index");
		}
		
		if (!result.hasErrors()) {
			String message = this.eventService.save(event);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/event/index");
		}
		else {
			return new ModelAndView("redirect:/event/add");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Integer id) {
		ModelAndView view = new ModelAndView("/event/edit");
		EventModel event = this.eventService.getEventFindById(id);
		if(event.getStatus()==2 || event.getStatus()==3) {
			return view;
		}
		else if(event.getStatus()==0 || event.getStatus()==1) {
			String title = "Edit Event - " + event.getEventName() + "(" + event.getCode() + ")";
			view.addObject("event", event);
			view.addObject("title", title);
			return view;
		}
		return view;
	}
	
	@GetMapping("details/{id}")
	public ModelAndView details(@PathVariable("id") Integer id) {
		ModelAndView views = new ModelAndView("/event/index");
		EventModel event = this.eventService.getEventFindById(id);
		if(event.getStatus()==1) {
			ModelAndView view = new ModelAndView("/event/details");
			String title = "View Event - " + event.getEventName() + "(" + event.getCode() + ")";
			view.addObject("event", event);
			view.addObject("title", title);
			return view;
		}
		if(event.getStatus()==2 && event.getAssignTo()==null) {
			ModelAndView view = new ModelAndView("/event/details2");
			List<EmployeeDTO> employeeList = this.eventService.getDDLEmployee();
			String title = "View Event - " + event.getEventName() + "(" + event.getCode() + ")";
			view.addObject("event", event);
			view.addObject("employeeList", employeeList);
			view.addObject("title", title);
			return view;
		}
		return views;
	}
	
	@PostMapping("approvement")
	public ModelAndView approvement(@ModelAttribute EventModel event, BindingResult result, HttpSession session) {
 		if(!result.hasErrors()) {
			String message = this.eventService.save(event);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/event/index");
		}
		else {
			return new ModelAndView("redirect:/event/details");
		}
	}
	
	@GetMapping("rejected/{id}")
	public ModelAndView rejected(@PathVariable("id") Integer id) {
		ModelAndView view = new ModelAndView("/event/rejected");
		EventModel event = this.eventService.getEventFindById(id);
		String title = "Event - " + event.getEventName() + "Reject Reason";
		view.addObject("event", event);
		view.addObject("title", title);
		return view;
	}
		
	@GetMapping("request/{id}")
	public ModelAndView request(@PathVariable("id") Integer id) {
		ModelAndView view = new ModelAndView("/event/delete");
		EventModel event = this.eventService.getEventFindById(id);
		if(event.getStatus()==0 || event.getStatus()==2) {
			String title = "Approval Event - " + event.getEventName() + "(" + event.getCode() + ")";
			view.addObject("event", event);
			view.addObject("title", title);
			return view;
		}
		return view;
	}
	
	@GetMapping("close/{id}")
	public ModelAndView close(@PathVariable("id") Integer id) {
		ModelAndView view = new ModelAndView("/event/closed");
		EventModel event = this.eventService.getEventFindById(id);
		String title = "Edit Event - " + event.getEventName() + "(" + event.getCode() + ")";
		view.addObject("event", event);
		view.addObject("title", title);
		return view;
	}
}
