package AssignmentJava;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import AssignmentJava.Dto.SouvenirSearchFormDTO;
import AssignmentJava.Dto.UnitDTO;
import AssignmentJava.model.SouvenirModel;
import AssignmentJava.service.SouvenirService;

@Controller
@RequestMapping("/souvenir")
public class SouvenirController {

	@Autowired
	private SouvenirService souvenirService;
	
	@GetMapping("index")
	public ModelAndView index(@RequestParam("page") Optional<Integer> page, @ModelAttribute SouvenirSearchFormDTO searchForm, HttpSession session) {

		ModelAndView view = new ModelAndView("/souvenir/index");
		List<UnitDTO> unitList = this.souvenirService.getDDLUnit();

		Integer currentPage = page.orElse(1);
		Page<SouvenirModel> souvenirPage = this.souvenirService.getSouvenirWithPage(PageRequest.of(currentPage - 1, 2), searchForm);

		Integer totalPages = souvenirPage.getTotalPages();
		if (totalPages > 0) {
			List<Integer> pageNumbers = this.souvenirService.getPageNumbers(totalPages);
			view.addObject("pageNumbers", pageNumbers);
		}
		String message = null;
		if (session.getAttribute("message") != null) {
			message = (String) session.getAttribute("message");
		}
		SouvenirSearchFormDTO searchFormDTO = searchForm;
		view.addObject("searchForm", searchFormDTO);
		view.addObject("unitList", unitList);
		view.addObject("souvenirPage", souvenirPage);
		view.addObject("message", message);
		session.removeAttribute("message");
		return view;
	}
	
	@GetMapping("add")
	public ModelAndView add() {
		ModelAndView view = new ModelAndView("/souvenir/add");
		SouvenirModel souvenir = new SouvenirModel();
		List<UnitDTO> unitList = this.souvenirService.getDDLUnit();
		view.addObject("souvenir", souvenir);
		view.addObject("unitList", unitList);
		return view;
	}

	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute SouvenirModel souvenir, BindingResult result, HttpSession session) {
		if (!result.hasErrors()) {
			String message = this.souvenirService.save(souvenir);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/souvenir/index");
		} else {
			return new ModelAndView("redirect:/souvenir/add");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Integer id) {
		ModelAndView view = new ModelAndView("/souvenir/edit");
		SouvenirModel souvenir = this.souvenirService.getSouvenirFindById(id);
		List<UnitDTO> unitList = this.souvenirService.getDDLUnit();
		String title = "Edit Souvenir - " + souvenir.getName() + " ("
				+ souvenir.getCode() + ")";
		view.addObject("souvenir", souvenir);
		view.addObject("title", title);
		view.addObject("unitList", unitList);
		return view;
	}
	
	@GetMapping("details/{id}")
	public ModelAndView details(@PathVariable("id") Integer id) {
		ModelAndView view = new ModelAndView("/souvenir/details");
		SouvenirModel souvenir = this.souvenirService.getSouvenirFindById(id);
		String title = "View Souvenir - " + souvenir.getName() + " (" + souvenir.getCode() + ")";
		view.addObject("souvenir", souvenir);
		view.addObject("title", title);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Integer id) {
		ModelAndView view = new ModelAndView("/souvenir/delete");
		SouvenirModel souvenir = new SouvenirModel();
		souvenir.setId(id);
		view.addObject("souvenir", souvenir);
		return view;
	}
	
	@PostMapping("deletedata")
	public ModelAndView deletedata(@ModelAttribute SouvenirModel souvenir, BindingResult result, HttpSession session) {
		if (!result.hasErrors()) {
			String message = this.souvenirService.delete(souvenir);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/souvenir/index");
		} else {
			return new ModelAndView("redirect:/souvenir/delete");
		}
	}
	
}
