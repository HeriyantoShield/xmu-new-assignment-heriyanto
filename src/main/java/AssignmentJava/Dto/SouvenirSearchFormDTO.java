package AssignmentJava.Dto;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class SouvenirSearchFormDTO {

	private String souvenirCode;
	
	private String souvenirName;
	
	private Integer UnitName;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate createdDate;
	
	private String createdBy;

	public String getSouvenirCode() {
		return souvenirCode;
	}

	public void setSouvenirCode(String souvenirCode) {
		this.souvenirCode = souvenirCode;
	}

	public String getSouvenirName() {
		return souvenirName;
	}

	public void setSouvenirName(String souvenirName) {
		this.souvenirName = souvenirName;
	}

	public Integer getUnitName() {
		return UnitName;
	}

	public void setUnitName(Integer unitName) {
		UnitName = unitName;
	}

	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
		
}
