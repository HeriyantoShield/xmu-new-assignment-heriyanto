package AssignmentJava;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import AssignmentJava.Dto.CompanyDTO;
import AssignmentJava.Dto.EmployeeSearchFormDTO;
import AssignmentJava.model.EmployeeModel;
import AssignmentJava.service.EmployeeService;

@Controller
@RequestMapping("/employee")
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping("index")
	public ModelAndView index(@RequestParam("page") Optional<Integer> page, @ModelAttribute EmployeeSearchFormDTO searchForm, HttpSession session) {

		ModelAndView view = new ModelAndView("/employee/index");
		List<CompanyDTO> companyList = this.employeeService.getDDLCompany();

		Integer currentPage = page.orElse(1);
		Page<EmployeeModel> employeePage = this.employeeService.getEmployeeWithPage(PageRequest.of(currentPage - 1, 2), searchForm);

		Integer totalPages = employeePage.getTotalPages();
		if (totalPages > 0) {
			List<Integer> pageNumbers = this.employeeService.getPageNumbers(totalPages);
			view.addObject("pageNumbers", pageNumbers);
		}
		String message = null;
		if (session.getAttribute("message") != null) {
			message = (String) session.getAttribute("message");
		}
		EmployeeSearchFormDTO searchFormDTO = searchForm;
		view.addObject("searchForm", searchFormDTO);
		view.addObject("companyList", companyList);
		view.addObject("employeePage", employeePage);
		view.addObject("message", message);
		session.removeAttribute("message");
		return view;
	}
	
	@GetMapping("add")
	public ModelAndView add() {
		ModelAndView view = new ModelAndView("/employee/add");
		EmployeeModel employee = new EmployeeModel();
		List<CompanyDTO> companyList = this.employeeService.getDDLCompany();
		view.addObject("employee", employee);
		view.addObject("companyList", companyList);
		return view;
	}

	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute EmployeeModel employee, BindingResult result, HttpSession session) {
		if (!result.hasErrors()) {
			String message = this.employeeService.save(employee);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/employee/index");
		} else {
			return new ModelAndView("redirect:/employee/add");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Integer id) {
		ModelAndView view = new ModelAndView("/employee/edit");
		EmployeeModel employee = this.employeeService.getEmployeeFindById(id);
		List<CompanyDTO> companyList = this.employeeService.getDDLCompany();
		String title = "Edit Employee - " + employee.getFirstName() + " " + employee.getLastName() + " ("
				+ employee.getCode() + ")";
		view.addObject("employee", employee);
		view.addObject("title", title);
		view.addObject("companyList", companyList);
		return view;
	}
	
	@GetMapping("details/{id}")
	public ModelAndView details(@PathVariable("id") Integer id) {
		ModelAndView view = new ModelAndView("/employee/details");
		EmployeeModel employee = this.employeeService.getEmployeeFindById(id);
		String title = "View Employee - " + employee.getFirstName() + " " + employee.getLastName() + " ("
				+ employee.getCode() + ")";
		view.addObject("employee", employee);
		view.addObject("title", title);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Integer id) {
		ModelAndView view = new ModelAndView("/employee/delete");
		EmployeeModel employee = new EmployeeModel();
		employee.setId(id);
		view.addObject("employee", employee);
		return view;
	}
	
	@PostMapping("deletedata")
	public ModelAndView deletedata(@ModelAttribute EmployeeModel employee, BindingResult result, HttpSession session) {
		if (!result.hasErrors()) {
			String message = this.employeeService.delete(employee);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/employee/index");
		} else {
			return new ModelAndView("redirect:/employee/delete");
		}
	}
	
}
